from django.db import models

# Create your models here.


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100, default="Default Closet Name")
    bin_number = models.PositiveSmallIntegerField(default=0)
    bin_size = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return str(self.bin_number)


class Shoes(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)

    bin_number = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.model_name
