from django.contrib import admin

# Register your models here.
from .models import Shoes, BinVO

admin.site.register(Shoes)
admin.site.register(BinVO)
