import React, { useEffect, useState } from 'react';

function ShoeForm(props) {
  const [manufacturer, setManufacturer] = useState("");
  const [modelName, setModelName] = useState("");
  const [color, setColor] = useState("");
  const [pictureUrl, setPictureUrl] = useState("");
  const [binNumber, setBinNumber] = useState("");
  const [bins, setBins] = useState([]);
  const [errorMessage, setErrorMessage] = useState("");

  const clearShoeList = () => {
    setManufacturer("");
    setModelName("");
    setColor("");
    setPictureUrl("");
    setBinNumber("");
  };

  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  };

  const handleModelNameChange = (event) => {
    const value = event.target.value;
    setModelName(value);
  };

  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  };

  const handlePictureUrlChange = (event) => {
    const value = event.target.value;
    setPictureUrl(value);
  };

  const handleBinNumberChange = (event) => {
    const value = event.target.value;
    setBinNumber(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      manufacturer: manufacturer,
      model_name: modelName,
      color: color,
      picture_url: pictureUrl,
      bin_number: binNumber
    };
    console.log("Data", data)
    const shoeURL = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        "Content-type": "application/json",
      },
    };

    const response = await fetch(shoeURL, fetchConfig);
    const responseData = await response.json();

    if (response.ok) {
      console.log(responseData);
      clearShoeList();
    } else {
      setErrorMessage(responseData.message);
    }
  };

  useEffect(() => {
    const getBins = async () => {
      const response = await fetch('http://localhost:8100/api/bins/');
      const data = await response.json();
      setBins(data.bins);
    };
    getBins();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new Shoe</h1>
          <div className="text-danger">{errorMessage}</div>
          <form id="create-shoe-form" onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input
                placeholder="manufacturer"
                required
                type="text"
                value={manufacturer}
                onChange={handleManufacturerChange}
                id="manufacturer"
                name="manufacturer"
                className="form-control"
              />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="model_name"
                required
                type="text"
                name="model_name"
                value={modelName}
                onChange={handleModelNameChange}
                id="model_name"
                className="form-control"
              />
              <label htmlFor="model_name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="color"
                required
                type="text"
                name="color"
                value={color}
                onChange={handleColorChange}
                id="color"
                className="form-control"
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="picture_url"
                required
                type="url"
                name="picture_url"
                value={pictureUrl}
                onChange={handlePictureUrlChange}
                id="picture_url"
                className="form-control"
              />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="form-floating mb-3">
            <select value={binNumber} onChange={(event => setBinNumber(event.target.value))} required id="bin" name="bin" className="form-select">
                  <option value="">Choose a bin</option>
                  {bins.map(bin => {
                                    return <option key={bin.id} value={bin.bin_number}>{bin.bin_number}</option>
                                })}
                </select>
            </div>
            <button type="submit" className="btn btn-primary">
              Create
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;