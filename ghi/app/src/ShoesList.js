import React, {useEffect, useState} from 'react';

function ShoesList(props) {
    const [shoes, setShoes] = useState([]);

    const fetchData  = async () => {
      const url = 'http://localhost:8080/api/shoes/';

      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setShoes(data.shoes);
        console.log(data.shoes)
      }
    }
    useEffect(() => {
      fetchData();
    }, []);

    const handleDelete = async (id) => {
      try {
        const response = await fetch(`http://localhost:8080/api/shoes/${id}`, {
          method: "DELETE",
        });
        if (response.ok) {
          fetchData()
        } else {
          console.log("Error")
        }
      } catch (error) {
        console.error("Error deleting shoe", error);
      }
    }

      return (
          <table className="table table-striped">
          <thead>
            <tr>
              <th>Manufacturer</th>
              <td>Model name</td>
            </tr>
          </thead>
          <tbody>
            {shoes.map((shoe, index) => {
              return (
                <tr key={`${shoe.id}-${index}`}>
                  <td>{ shoe.manufacturer }</td>
                  <td>{shoe.model_name }</td>
                  <td>
                    <button onClick={() => handleDelete(shoe.id)}>Delete</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      )
  }

  export default ShoesList;