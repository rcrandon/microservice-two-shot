# Wardrobify

Team:

- Nariman - shoes
- Randall - hats

## Design

## Shoes microservice

1. Shoe Model Creation: I created a model for our Shoe resources in Django, it includes the fields for the shoe's manufacturer, model name, color, an image URL, and a bin ID for its location in the wardrobe.

2. Shoes API Creation: I set up a Django app for the Shoes API that can be accessed at port 8080. It supports the basic functions of listing all shoes, creating a new shoe entry, and deleting an existing shoe using the GET, POST, and DELETE methods respectively.

3. Poller for Shoes: I built a separate poller application that fetches data about shoe locations (Bins) from the Wardrobe API. This poller ensures the bin data associated with each shoe is kept up-to-date in our API.

4. Wardrobe API Integration: I've linked our Shoe resources with the Wardrobe API's Bin resources through the bin ID field in our Shoe model. This allows our poller to continuously pull updated Bin data from the Wardrobe API, ensuring our shoe location data stays current.

5. React Components for Shoes: On the front-end, I've developed a React component that displays a list of all shoes and their associated details. I've also designed a form for adding new shoes and a function for deleting existing ones.

6. React Routing: I've routed our newly created shoe components with the existing navigation links in our React app, making them accessible directly from the main navigation.

7. Git and Docker Management: Throughout the project, I've been consistently pulling and pushing changes from GitLab to ensure we're all on the same page. I've also efficiently handled Docker services, rebuilding and restarting them when necessary.

## Hats microservice

Hats Model:

1. Developed a Django model for Hat resources, including attributes for fabric, style name, color, a picture URL, and a location ID to indicate where it is in the wardrobe.
   Hats API:

2. Created a Django application serving a RESTful API on port 8090.
   Implemented GET, POST, and DELETE methods to list all hats, create a new hat, and delete a hat respectively.
   Hats Poller:

3. Developed a poller application that regularly fetches Location data from the Wardrobe API.
   Integrated this polling mechanism into the Hats API, allowing the location data of each hat to stay updated.
   Integration with Wardrobe API:

4. Associated each Hat resource with a Location resource from the Wardrobe API by storing the Location ID in the Hat model.
   Enabled the Hats Poller to fetch updated Location data from the Wardrobe API regularly, keeping the location data in the Hat resources updated.
   React Components:

5. Developed a React component to display a list of all hats and their details.
   Created a form component in React to add a new hat.
   Implemented functionality in React to delete a hat from the list.
   Routing:

6. Set up routing in the React application to navigate to the newly created components from existing navigation links.
   Teamwork and Docker Management:

7. Regularly pulled and pushed changes from/to GitLab, successfully managing any merge conflicts.
   Managed Docker services effectively by building and bringing up the services whenever there were changes.
